﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIController : MonoBehaviour
{
    [SerializeField] private GameObject _colecaoP1;
    [SerializeField] private GameObject _colecaoP2;
    public void AbrirFecharColecao(Animator colecao)
    {
        if (!colecao.gameObject.activeSelf)
        {
            _colecaoP1.SetActive(false);
            _colecaoP2.SetActive(false);
            colecao.gameObject.SetActive(true);
        }
        else
        {
            _colecaoP1.SetActive(false);
            _colecaoP2.SetActive(false);
            colecao.gameObject.SetActive(false);
        }
    }
}
