﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ITipoAnimal
{
   MACACO = 0,
   CACHORRO = 1,
   VACA = 2,
   ELEFANTE = 3,
   PINGUIM = 4,
   COELHO = 5,
   GATO = 6
}
