﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnimalEncontradoController : MonoBehaviour
{
    [Tooltip("Pegar página do álbum dentro do canvas")]
    [SerializeField] private GameObject _pagina1 = default;
    [Tooltip("Pegar página do álbum dentro do canvas")]
    [SerializeField] private GameObject _pagina2 = default;

    private List<TipoDeAnimal> _figuras;

    private void Start()
    {
        PreencherFiguras();
    }

    public void HabilitarAnimal(ITipoAnimal animal)
    {
        TipoDeAnimal a = GetAnimalPorTipo(animal);

        if (a == null) return;

        Color newColor = a.ImageComponent.color;
        newColor.a = 255;
        a.ImageComponent.color = newColor;
    }

    private TipoDeAnimal GetAnimalPorTipo(ITipoAnimal tipo)
    {
        foreach (TipoDeAnimal t in _figuras)
        {
            if (t.Tipo == tipo)
            {
                return t;
            }
        }
        return null;
    }

    private void PreencherFiguras()
    {
        _figuras = new List<TipoDeAnimal>();
        foreach (TipoDeAnimal i in _pagina1.GetComponentsInChildren<TipoDeAnimal>())
        {
            _figuras.Add(i);
        }
        foreach (TipoDeAnimal i in _pagina2.GetComponentsInChildren<TipoDeAnimal>())
        {
            _figuras.Add(i);
        }
    }
}
