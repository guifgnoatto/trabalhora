﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "TipoAnimal", menuName = "TipoAnimal/Animal")]
public class ComidaHabitatAnimal : ScriptableObject
{
    [SerializeField] ITipoAnimal _tipoAnimal = default;
    [SerializeField] private Sprite _comidaCerta = default;
    [SerializeField] private List<Sprite> _outrasComidas = default;

    public ITipoAnimal TipoAnimal { get => _tipoAnimal; set => _tipoAnimal = value; }
    public Sprite ComidaCerta { get => _comidaCerta; set => _comidaCerta = value; }
    public List<Sprite> OutrasComidas { get => _outrasComidas; set => _outrasComidas = value; }
}
