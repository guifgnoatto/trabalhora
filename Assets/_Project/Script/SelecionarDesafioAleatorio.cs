﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelecionarDesafioAleatorio : MonoBehaviour
{
    [SerializeField] private List<GameObject> _desafios;

    private void Start()
    {
        DesativarDesafios();
        AtivarDesafioAleatorio();
        GameSystem.Instance.Player1StartedToPlay += TrocaTurno;
        GameSystem.Instance.Player2StartedToPlay += TrocaTurno;
    }

    private void AtivarDesafioAleatorio()
    {
        int desafioIndex = Random.Range(0, _desafios.Count);
        _desafios[desafioIndex].SetActive(true);
    }


    private void DesativarDesafios()
    {
        foreach (GameObject desafio in _desafios)
        {
            desafio.SetActive(false);
        }
    }

    private void TrocaTurno()
    {
        DesativarDesafios();
        AtivarDesafioAleatorio();
    }
}
