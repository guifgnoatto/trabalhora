﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSystem : MonoBehaviour
{
	public static GameSystem Instance;

	public event Action Player1StartedToPlay;
	public event Action Player1FinishedToPlay;
	public event Action Player2StartedToPlay;
	public event Action Player2FinishedToPlay;
	public event Action PausedGame;
	public event Action ResumedGame;
	
	public void StartPlayer1Gameplay()
	{
		Player1StartedToPlay.Invoke();
	}
	
	public void Finishlayer1Gameplay()
	{
		if (Player1FinishedToPlay != null)
		{
			Player1FinishedToPlay.Invoke();
		}
	}
	
	public void StartPlayer2Gameplay()
	{
		Player2StartedToPlay.Invoke();
	}

	public void Finishlayer2Gameplay()
	{
		if (Player2FinishedToPlay != null)
		{
			Player2FinishedToPlay.Invoke();
		}
	}

	public void PauseGame()
	{
		if (PausedGame != null)
		{
			PausedGame.Invoke();	
		}
	}

	public void ResumeGame()
	{
		if (ResumedGame != null)
		{
			ResumedGame.Invoke();
		}
	}
	private void Awake()
	{
		if (Instance != null)
		{
			Destroy(gameObject);
		}
		else
		{
			Instance = this;
			DontDestroyOnLoad(this);
			SaveSystem.Initialize();
		}
	}
}
