﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TipoDeAnimal : MonoBehaviour
{
    [SerializeField] private ITipoAnimal _tipo;
    [SerializeField] private Image _imageComponent;

    private void Awake()
    {
        _imageComponent = GetComponent<Image>();
    }

    public ITipoAnimal Tipo { get => _tipo; set => _tipo = value; }
    public Image ImageComponent { get => _imageComponent; set => _imageComponent = value; }
}
