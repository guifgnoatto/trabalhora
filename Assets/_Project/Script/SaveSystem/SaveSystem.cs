﻿using System.IO;
using UnityEngine;


public static class SaveSystem
{
	//private static readonly string _saveFolder = Application.dataPath + "/_Project/SaveFiles/";
	private static readonly string _saveFolder = Application.persistentDataPath + "/SaveFiles/";

	public static void Initialize()
	{
		if (!Directory.Exists(_saveFolder))
		{
			Directory.CreateDirectory(_saveFolder);
		}
	}

	public static void Save(string fileName, string jsonFile)
	{
		File.WriteAllText(_saveFolder + fileName, jsonFile);
	}

	public static string Load(string fileName)
	{
		if (File.Exists(_saveFolder + fileName))
		{
			string jsonFile = File.ReadAllText(_saveFolder + fileName);
			return jsonFile;
		}
		else
		{
			return null;
		}
	}
}