﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[Serializable]
public class SaveObject
{
    public int Value = default;
    public string Name = default;
}