﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameStarter : BaseHUD
{
   [SerializeField]
   private List<BasePanel> _panels = default;
  
   public void StartPlayer1Gameplay()
   {
      ActivateOnlyPanelsOfType(PanelType.GAMEPLAY_HUD, _panels);
      GameSystem.Instance.StartPlayer1Gameplay();
   }
   
   public void StartPlayer2Gameplay()
   {
      ActivateOnlyPanelsOfType(PanelType.GAMEPLAY_HUD, _panels);
      GameSystem.Instance.StartPlayer2Gameplay();
   }

   public void PauseGame()
   {
      GameSystem.Instance.PauseGame();
      ActivateOnlyPanelsOfType(PanelType.PAUSE_HUD, _panels);
      Time.timeScale = 0;
   }

   public void ResumeGame()
   {
      GameSystem.Instance.ResumeGame();
      ActivateOnlyPanelsOfType(PanelType.GAMEPLAY_HUD, _panels);
      Time.timeScale = 1;
   }
   
   public void GoToMenu()
   {
      SceneManager.LoadScene(0);
   }

   private void Start()
   {
      ShowPlayer1Panel();
      GameSystem.Instance.Player1FinishedToPlay += ShowPlayer2Panel;
      GameSystem.Instance.Player2FinishedToPlay += ShowPlayerWinPanel;
   }

   private void ShowPlayer1Panel()
   {
      ActivateOnlyPanelsOfType(PanelType.PLAYER_1_PLAY_HUD, _panels);
   }
   
   private void ShowPlayer2Panel()
   {
      ActivateOnlyPanelsOfType(PanelType.PLAYER_2_PLAY_HUD, _panels);
   }

   private void ShowPlayerWinPanel()
   {
      ActivateOnlyPanelsOfType(PanelType.END_GAME_HUD, _panels);
   }
}
