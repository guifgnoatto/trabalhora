﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class SliderPanel : BasePanel
{
	[SerializeField]
	protected Slider _slider = default;
	[SerializeField]
	private TextPanel _textRepresentation = default;

	public SaveObject _mySave;

	public void Initialize(SaveObject save, float minValue, float maxValue)
	{
		if (_textRepresentation != null)
		{
			_textRepresentation.Initialize();
		}

		_slider = GetComponent<Slider>();
		_slider.maxValue = maxValue;
		_slider.minValue = minValue;

		_mySave = save;
		SetValue(save.Value);
	}

	public void SetValue(float value)
	{
		_slider.value = value;
		_mySave.Value = Convert.ToInt32(value);

		if (_textRepresentation != null)
		{
			_textRepresentation.WriteText(_mySave.Value.ToString());
		}
	}
}
