﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuFlow : BaseHUD
{
	[SerializeField]
	private List<BasePanel> _panels;
	[SerializeField]
	private SliderPanel _gameTimer = default;
	[SerializeField]
	private SoundSliderPanel _mainVolumeSlider = default;

	private readonly int _volumeMinValue = -80;
	private readonly int _volumeMaxValue = 0;
	private readonly int _gameTimerMinValue = 1;
	private readonly int _gameTimerMaxValue = 15;
	private readonly string _gameTimerFile = "GameTimer";
	private readonly string _mainVolumeFile = "MainVolume";
	private readonly string _settingsFile = "Settings.txt";
	
	public void AbrirMenuPrincipal()
	{
		ActivateOnlyPanelsOfType(PanelType.MAIN_MENU, _panels);
	}

	public void AbrirMenuPlay()
	{
		ActivateOnlyPanelsOfType(PanelType.GAMEPLAY_MENU, _panels);
	}

	public void AbrirMenuDeOpcoes()
	{
		ActivateOnlyPanelsOfType(PanelType.OPTIONS_MENU, _panels);
	}

	public void AbrirFelinoScene()
	{
		SceneManager.LoadScene(1);
	}

	public void QuitGame()
	{
		Application.Quit();
	}

	public void Save()
	{
		UpdateSavior(_gameTimer._mySave);
		UpdateSavior(_mainVolumeSlider._mySave);
		SaveSettings();
	}

	public void Load()
	{
		LoadSettings();
	}

	protected override void LoadSettings()
	{
		string savesJson = SaveSystem.Load(_settingsFile);

		if (savesJson == null)
		{
			StartFirstSettings();
			savesJson = SaveSystem.Load(_settingsFile);
		}
		_saves = JsonUtility.FromJson<SettingsSave>(savesJson);
	}

	private void Start()
	{
		LoadSettings();
		UpdateSettings();
		AbrirMenuPrincipal();
	}
	
	private void StartFirstSettings()
	{
		List<SaveObject> firstSaves = new List<SaveObject>()
		{
			new SaveObject {Name = _mainVolumeFile, Value = 0},
			new SaveObject {Name = _gameTimerFile, Value = 10},
		};
		_saves = new SettingsSave {Saves = firstSaves};
		SaveSettings();
	}

	private void UpdateSettings()
	{
		if (GetSave(_gameTimerFile) != null)
		{
			_gameTimer.Initialize(GetSave(_gameTimerFile), _gameTimerMinValue, _gameTimerMaxValue);
		}
		if (GetSave(_mainVolumeFile) != null)
		{
			_mainVolumeSlider.Initialize(GetSave(_mainVolumeFile), _volumeMinValue, _volumeMaxValue);
		}
		
	}
}
