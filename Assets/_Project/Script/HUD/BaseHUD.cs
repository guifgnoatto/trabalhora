﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseHUD : MonoBehaviour
{
   protected readonly string _savesFile = "Settings.txt";
   protected SettingsSave _saves;
   
   protected void SetPanelActiveTo(BasePanel panel, bool state)
   {
      panel.gameObject.SetActive(state);
   }
   
   protected void DesactivatePanelWithTimer(BasePanel panel, float timer)
   {
      IEnumerator DesactivatePanel()
      {
         yield return new WaitForSeconds(timer);
         panel.gameObject.SetActive(false);
      }
   }

   protected void ActivateOnlyPanelsOfType(PanelType type, List<BasePanel> panels)
   {
      foreach (BasePanel panel in panels)
      {
         if (panel.Type == type)
         {
            SetPanelActiveTo(panel,true);
         }
         else
         {
            SetPanelActiveTo(panel,false);
         }
      }
   }

   protected void UdpateText(TextPanel textPanel, string newTextContent)
   {
      textPanel.WriteText(newTextContent);
   }

   protected void UdpateSlider(SliderPanel sliderPanel, float newSliderValue)
   {
      sliderPanel.SetValue(newSliderValue);
   }
		
   protected virtual void LoadSettings()
   {
      string settingsJson = SaveSystem.Load(_savesFile);
            
      if (settingsJson == null)
      {
         Debug.Log("There are no saves");
         return;
      }
      _saves = JsonUtility.FromJson<SettingsSave>(settingsJson);
   }
        
   protected SaveObject GetSave(string name)
   {
      foreach (SaveObject save in _saves.Saves )
      {
         if (save.Name == name)
         {
            return save;
         }
      }
      return null;
   }
		
   protected void SaveSettings()
   {
      string jsonString = JsonUtility.ToJson(_saves);
      SaveSystem.Save(_savesFile, jsonString);
   }

   protected void UpdateSavior(SaveObject saveToUpdate)
   {
      foreach (SaveObject save in _saves.Saves)
      {
         if (saveToUpdate.Name == save.Name)
         {
            save.Value = saveToUpdate.Value;
         }
      }
   }
}
