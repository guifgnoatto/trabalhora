﻿
using UnityEngine;
using UnityEngine.Audio;

public class SoundSliderPanel : SliderPanel
{
    [SerializeField]
    private AudioMixer _audioMixer = default;

    private readonly string _mainVolume = "GameVolume";

    public void SetMainVolume(float volume)
    {
        SetValue(volume);
        _audioMixer.SetFloat(_mainVolume, volume);
    }
}
