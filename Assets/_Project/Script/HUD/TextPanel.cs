﻿using TMPro;
using UnityEngine;

public class TextPanel : BasePanel
{
	[SerializeField]
	private TextMeshProUGUI _text = default;

	public void WriteText(string textToBeWritten)
	{
		_text.text = textToBeWritten;
	}

	public void Initialize()
	{
		if (_text == null)
		{
			_text = GetComponent<TextMeshProUGUI>();
		}
	}
}
