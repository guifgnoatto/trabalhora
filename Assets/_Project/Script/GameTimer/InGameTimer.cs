﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class InGameTimer : MonoBehaviour
{
    public event Action TimeIsOver;

    [SerializeField]
    private TextPanel _timer = default;
    [SerializeField]
    private TextMeshProUGUI _firstPlace = default;
    [SerializeField]
    private TextMeshProUGUI _secondPlace = default;
    [SerializeField] private int _actualTimer;
    
    private SettingsSave _saves;
    private SettingsSave _timesSaves;
    private int _initialTimer;
    private int _player1TotalTime;
    private int _player2TotalTime;
    private bool Player1IsPlaying = false;
    private bool Player2IsPlaying = false;

    private readonly string _gameTimerFile = "GameTimer";
    private readonly string _settingsFile = "Settings.txt";
    private readonly string _player1nameInFile = "Player1";
    private readonly string _player2nameInFile = "Player2";
    
    public void Player1StartToRunTimer()
    {
        Player1IsPlaying = true;
        ResetTimer();
        StartCoroutine(RunTimer());
    }
    
    public void Player2StartToRunTimer()
    {
        Player2IsPlaying = true;
        ResetTimer();
        StartCoroutine(RunTimer());
    }

    public void ResetTimer()
    {
        _actualTimer = _initialTimer;
    }
    
    private void Start()
    {
        _timer.Initialize();
        LoadTimer();
        ResetTimer();
        GameSystem.Instance.Player1StartedToPlay += Player1StartToRunTimer;
        GameSystem.Instance.Player2StartedToPlay += Player2StartToRunTimer;
        GameSystem.Instance.Player2FinishedToPlay += Player2FinishedGameplay;
        GameSystem.Instance.Player1FinishedToPlay += Player1FinishedGameplay;
    }

    private void LoadTimer()
    {
        string saves = SaveSystem.Load(_settingsFile);
        _saves = JsonUtility.FromJson<SettingsSave>(saves);
        
        foreach (SaveObject save in _saves.Saves)
        {
            if (save.Name == _gameTimerFile)
            {
                _initialTimer = save.Value * 60;
                return;
            }
        }
        Debug.Log("There is no timer saved");
    }
    
    private IEnumerator RunTimer()
    {
        _timer.WriteText(_actualTimer.ToString());
        yield return new WaitForSeconds(1);
        if (_actualTimer > 0)
        {
            _actualTimer -= 1;
            StartCoroutine(RunTimer());
        }
        else if (Player1IsPlaying)
        {
            GameSystem.Instance.Finishlayer1Gameplay();
        }
        else if (Player2IsPlaying)
        {
            GameSystem.Instance.Finishlayer2Gameplay();
        }
    }

    private void SetScoreTexts()
    {
        if (_player2TotalTime > _player1TotalTime)
        {
            _firstPlace.text = "1. Jogador 1 " + _player1TotalTime;
            _secondPlace.text = "2. Jogador 2 " + _player2TotalTime;
        }
        else
        {
            _firstPlace.text = "1. Jogador 2 " + _player2TotalTime;
            _secondPlace.text = "2. Jogador 1 " + _player1TotalTime;
        }
    }
    
    private void UpdateSave(string name, int value)
    {
        foreach (SaveObject save in _saves.Saves )
        {
            if (save.Name == name)
            {
                save.Value = value;
                return;
            }
        }
        SaveObject firstSave = new SaveObject(){Name = name,Value = value};
        _saves.Saves.Add(firstSave);
    }
		
    private void SaveSettings()
    {
        string jsonString = JsonUtility.ToJson(_saves);
        SaveSystem.Save(_settingsFile, jsonString);
    }

    private void Player1FinishedGameplay()
    {
        UpdateSave(_player1nameInFile,_initialTimer - _actualTimer);
        SaveSettings();
        _player1TotalTime =  _initialTimer - _actualTimer;
        Player1IsPlaying = false;
    }

    private void Player2FinishedGameplay()
    {
        UpdateSave(_player2nameInFile, _initialTimer - _actualTimer);
        SaveSettings();
        _player2TotalTime =  _initialTimer - _actualTimer;
        Player2IsPlaying = false;
        SetScoreTexts();
    }
}
