﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "TipoSom", menuName = "TipoSom/Sons")]
public class TipoDeSom : ScriptableObject
{
    [SerializeField] private ITipoAnimal _tipoAnimal = default;
    [SerializeField] private AudioClip _audioCerto = default;
    [SerializeField] private List<AudioClip> _audiosErrados = default;

    public ITipoAnimal TipoAnimal { get => _tipoAnimal; set => _tipoAnimal = value; }
    public AudioClip AudioCerto { get => _audioCerto; set => _audioCerto = value; }
    public List<AudioClip> AudiosErrados { get => _audiosErrados; set => _audiosErrados = value; }
}
