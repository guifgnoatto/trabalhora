﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DesafioSom : MonoBehaviour
{
    [SerializeField] private AnimalEncontradoController _animalControllerP1 = default;
    [SerializeField] private AnimalEncontradoController _animalControllerP2 = default;
    [SerializeField] private TipoDeSom _tipoDeSom = default;
    [SerializeField] private GameObject _acertouText = default;
    [SerializeField] private GameObject _errouText = default;
    [SerializeField] private List<Button> _audioButtons = default;
    [SerializeField] private List<Button> _selectButtons = default;
    private bool _player1Turn;
    private bool _player2Turn;

    private List<AudioClip> _audiosUtilizados;
    private int _indexBotaoCorreto;

    public void ChecarAudioCorreto(Button b)
    {
        if (b == _selectButtons[_indexBotaoCorreto])
        {
            _acertouText.SetActive(true);
            _errouText.SetActive(false);
            if (_player1Turn)
            {
                _animalControllerP1.HabilitarAnimal(_tipoDeSom.TipoAnimal);
            }
            else if (_player2Turn)
            {
                _animalControllerP2.HabilitarAnimal(_tipoDeSom.TipoAnimal);
            }
        }
        else
        {
            _errouText.SetActive(true);
            _acertouText.SetActive(false);
        }
    }

    public void TocarAudio(Button b)
    {
        b.GetComponent<AudioSource>().Play();
    }

    private void Awake()
    {
        _player1Turn = false;
        _player2Turn = false;
        _audiosUtilizados = new List<AudioClip>();
        _indexBotaoCorreto = -1;
        MisturarSons();
        GameSystem.Instance.Player1StartedToPlay += TrocarTurnoP1;
        GameSystem.Instance.Player2StartedToPlay += TrocarTurnoP2;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            MisturarSons();
        }
    }

    private void TrocarTurnoP1()
    {
        ResetarDesafio();
        MisturarSons();
        _player1Turn = true;
        _player2Turn = false;
    }

    private void TrocarTurnoP2()
    {
        ResetarDesafio();
        MisturarSons();
        _player2Turn = true;
        _player1Turn = false;
    }

    private void ResetarDesafio()
    {
        _acertouText.SetActive(false);
        _errouText.SetActive(false);
    }

    private void MisturarSons()
    {
        LimparSons();

        int counter = 0;
        while (counter <= _audioButtons.Count - 1)
        {
            int respostaCorreta = Random.Range(0, _audioButtons.Count);

            if ((respostaCorreta == 0 || counter == _audioButtons.Count - 1) &&
                !_audiosUtilizados.Contains(_tipoDeSom.AudioCerto))
            {
                _audioButtons[counter].GetComponent<AudioSource>().clip = _tipoDeSom.AudioCerto;
                _audiosUtilizados.Add(_tipoDeSom.AudioCerto);
                _indexBotaoCorreto = counter;
            }
            else if (_audioButtons[counter].GetComponent<AudioSource>().clip == null)
            {
            tryAgain:
                int spriteIndex = Random.Range(0, _tipoDeSom.AudiosErrados.Count);
                if (!_audiosUtilizados.Contains(_tipoDeSom.AudiosErrados[spriteIndex]))
                {
                    _audioButtons[counter].GetComponent<AudioSource>().clip = _tipoDeSom.AudiosErrados[spriteIndex];
                    _audiosUtilizados.Add(_tipoDeSom.AudiosErrados[spriteIndex]);
                }
                else
                    goto tryAgain;
            }
            counter++;
        }
    }

    private void LimparSons()
    {
        _errouText.SetActive(false);
        _acertouText.SetActive(false);
        foreach (Button b in _audioButtons)
        {
            b.GetComponent<AudioSource>().clip = null;
            _audiosUtilizados.Clear();
            _indexBotaoCorreto = -1;
        }
    }
}
