﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEngine;
using UnityEngine.UI;

public class DesafioComida : MonoBehaviour
{
    [SerializeField] private AnimalEncontradoController _animalControllerP1 = default;
    [SerializeField] private AnimalEncontradoController _animalControllerP2 = default;
    [SerializeField] private ComidaHabitatAnimal _tipoDeAnimal = default;
    [SerializeField] private GameObject _acertouText = default;
    [SerializeField] private GameObject _errouText = default;
    [SerializeField] private List<Button> _buttons = default;
    private List<Sprite> _spritesUtilizadas;
    private bool _player1Turn;
    private bool _player2Turn;

    public void ChecarRespostaCerta(Button b)
    {
        if (b.image.sprite == _tipoDeAnimal.ComidaCerta)
        {
            _acertouText.SetActive(true);
            _errouText.SetActive(false);
            if (_player1Turn)
            {
                _animalControllerP1.HabilitarAnimal(_tipoDeAnimal.TipoAnimal);
            }
            else if (_player2Turn)
            {
                _animalControllerP2.HabilitarAnimal(_tipoDeAnimal.TipoAnimal);
            }
        }
        else
        {
            _errouText.SetActive(true);
            _acertouText.SetActive(false);
        }
    }

    private void Awake()
    {
        _spritesUtilizadas = new List<Sprite>();
        MisturarSprites();
        GameSystem.Instance.Player1StartedToPlay += TrocarTurnoP1;
        GameSystem.Instance.Player2StartedToPlay += TrocarTurnoP2;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            MisturarSprites();
        }
    }

    private void TrocarTurnoP1()
    {
        ResetarDesafio();
        MisturarSprites();
        _player1Turn = true;
        _player2Turn = false;
    }

    private void TrocarTurnoP2()
    {
        ResetarDesafio();
        MisturarSprites();
        _player2Turn = true;
        _player1Turn = false;
    }

    private void ResetarDesafio()
    {
        _acertouText.SetActive(false);
        _errouText.SetActive(false);
    }

    private void MisturarSprites()
    {
        LimparSprites();

        int counter = 0;
        while (counter <= _buttons.Count - 1)
        {
            int respostaCorreta = Random.Range(0, _buttons.Count);

            if ((respostaCorreta == 0 || counter == _buttons.Count - 1) &&
                !_spritesUtilizadas.Contains(_tipoDeAnimal.ComidaCerta))
            {
                _buttons[counter].image.sprite = _tipoDeAnimal.ComidaCerta;
                _spritesUtilizadas.Add(_tipoDeAnimal.ComidaCerta);
            }
            else if (_buttons[counter].image.sprite == null)
            {
            tryAgain:
                int spriteIndex = Random.Range(0, _tipoDeAnimal.OutrasComidas.Count);
                if (!_spritesUtilizadas.Contains(_tipoDeAnimal.OutrasComidas[spriteIndex]))
                {
                    _buttons[counter].image.sprite = _tipoDeAnimal.OutrasComidas[spriteIndex];
                    _spritesUtilizadas.Add(_tipoDeAnimal.OutrasComidas[spriteIndex]);
                }
                else
                    goto tryAgain;
            }
            counter++;
        }
    }

    private void LimparSprites()
    {
        _errouText.SetActive(false);
        _acertouText.SetActive(false);
        foreach (Button b in _buttons)
        {
            b.image.sprite = null;
            _spritesUtilizadas.Clear();
        }
    }
}
